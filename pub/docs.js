var app={
    appid:'63ee6d44c824xx9765d5220619ae2c162p',
	init:function(templates,vars){
		$.fn.render.setTemplates(templates);
		if(vars) $.each(vars,function(i,v){
            app[i]=v;
        });
		app.uuid=window.uuid;
        if(app.isdev){//socket for real-time updating
            //load scroller data!
            modules.tools.loadScrollCache();
            //apply scroll
            document.documentElement.scrollTop=modules.tools.cacheScrollPosition('app','document',1);
            app.socket=io(app.domain+':3333?device='+app.uuid);//always gen a unique device id
            app.socket.on(app.env+'_'+app.site, app.onCodeUpdate);
        }
        if(window.location.hash=='#/'){
            window.location.hash='#/welcome'
        }
        $('body').on('click','.task',function(e){
            app.handleClick(e);
        });
        $(document).on('resize',function(){
            app.pagedata={
                hash:'',
                data:[]
            };
        });
        $('.page-loader').hide()
        $('.section-link').on('click',function(){
            $('.collapse').removeClass('collapse hold');
            modules.tools.throttle('preventScroll',1000,function(){});
        })
        $(document).on('scroll',function(){
            if(app.isdev) modules.tools.cacheScrollPosition('app','document');
            if(modules.tools.throttle('preventScroll')) return false;
            modules.tools.throttle('scroll',100,function(){
                //use this to set the corrent ?id= hash state
                var ph=window.location.hash.split('?');
                if(app.pagedata.hash!=ph[0]){
                    app.pagedata.hash=ph[0];
                    app.pagedata.data=[];
                    $('#main').find('[id]').each(function(i,v){
                        app.pagedata.data.push({
                            id:$(v).attr('id'),
                            position:$(v).position()
                        })
                    })
                }
                if(app.pagedata.data.length){
                    var currentTop=$(document).scrollTop();
                    var closest={
                        id:'',
                        index:0,
                        dist:100000
                    };
                    $.each(app.pagedata.data,function(i,v){
                        var diff=v.position.top-currentTop;
                        if(diff<0){
                            var dist=Math.abs(diff);
                            if(dist<closest.dist){
                                closest.dist=dist;
                                closest.id=v.id;
                                closest.index=i;
                            }
                        }
                    })
                    var sethash=ph[0];
                    if(closest.id){
                        if(closest.index==0){
                            sethash=ph[0];
                        }else{
                            sethash=ph[0]+'?id='+closest.id;
                        }
                    }
                    if(window.location.hash!=sethash){
                        window.location.hash=sethash;
                        $('.sidebar').find('.active.open').removeClass('active open');
                        $('.sidebar').find('[href="'+sethash+'"]').parent().addClass('active open');
                    }
                }
            },1);
        })
	},
    pagedata:{
        hash:'',
        data:[],
    },
    handleClick:function(e){
        var ele=$(e.target);
        switch(ele.data('task')){
            case 'gotohash':
                //clear out current content
                $('#main').html('');
                //scroll to top!
                $('html').scrollTop(0);
                window.location.hash=ele.data('intent');
            break;
            case 'link':
                //clear out current content
                window.open(ele.data('intent'),'_blank');
            break;
        }
    },
    onCodeUpdate:function(){
        window.location.reload();
    }
}