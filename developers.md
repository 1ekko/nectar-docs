<div class="landscapediv coverimg" style="background-image:url(https://s3.amazonaws.com/wearenectar/static/design.jpg">
</div>
<div style="padding-top:20px;">
Building things together is so much more fun!  

Everybody has different styles and flares that are all uniquely beautiful.  We should enable our technology to support this flavor and uniqueness.  So, Nectar offers an interface development starter kit for rapid (and easy) front end development.

We also offer a comprehensive oAuth2 system that provides secure access and sharing of your data.
</div>