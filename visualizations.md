<div class="landscapediv coverimg" style="background-image:url(https://s3.amazonaws.com/wearenectar/static/healing.jpeg">
</div>
<div style="text-align: left;">
	<p>
		Visualizations are windows of perception for how we can see, interpret, or gain insight into the world.  There are many ways that we can view data and information to glean information.  Heatmaps, Charts, graphs, etc.  As visualizations are a form of feedback and feedback improves evolution, we put high emphasis on creating and sharing visualization tools and techniques.
	</p>
	<p>
		We also place a high importance on user privacy.  Data and visualizations (as cool as they sometimes are) should not come before user privacy. 
	</p>
</div>
<div class="bottombuttons">
	<!-- <div class="task button pinkbtn button_left" data-task="gotohash" data-intent="#/welcome/vision">Back: Our Vision</div> -->
	<div class="task button bluebtn button_right" data-task="gotohash" data-intent="#/visualizations/maps">Next: Maps</div>
	<div class="clearboth"></div>
</div>