<div class="landscapediv coverimg" style="background-image:url(https://s3.amazonaws.com/wearenectar/static/hyperspace.jpg">
</div>
<div style="text-align: left;">
	<p>
		Nectar is currently in an <b>Open Beta</b>.  There are a lot of features and functionality available now and we will continue to roll out and improve features going forward. 
	</p>
	<p>
		Due to the rapidly evolving nature of Nectar, you may find some bugs here and there.  We aren't perfect, however we do the best we can when something is pointed out to us.  Because feedback is integral in any evolving system, there is a built in <span class="task" data-task="gotohash" data-intent="#/using_nectar/support" style="cursor: pointer;font-weight: bold">Support Tool</span> for you to contact us if you have any issues or feedback.
	</p>
	<p>
		We also highly encourage and promote <b>right relationship</b> with technology.  Connecting with friends, co-workers, potential dates, or whoever through the Internet has brought our world closer together in so many ways, however, spending too much time on computers or only relating through computers is not healthy.  In the same way that food can be good for you, but if you eat too much of it, it isn't.
	</p>
	<p>
		So, lets step forward more consciously in our relationship with technology so that we can walk hand in hand with our ally to a better world.
	</p>
</div>
<div class="bottombuttons">
	<!-- <div class="task button pinkbtn button_left" data-task="gotohash" data-intent="#/welcome/vision">Back: Our Vision</div> -->
	<div class="task button bluebtn button_right" data-task="gotohash" data-intent="#/using_nectar/overview">Next: Overview</div>
	<div class="clearboth"></div>
</div>