<div class="table" style="height:100vh">
	<div class="tcell">
		<div>
			<img src="https://wearenectar.s3.amazonaws.com/static/metadrop_home.png" style="width:250px;">
		</div>
		<div style="font-size:18px">
			<div>A Planetary Vision for</div>
			<div><b>Conscious Evolution</b></div>
		</div>
		<div class="bottombuttons" style="margin-top:50px;">
			<!-- <div class="task button pinkbtn button_left" data-task="gotohash" data-intent="#/welcome/intro">Back: Intro</div> -->
			<div class="task button bluebtn button_right" data-task="gotohash" data-intent="#/welcome/intro">Next: Intro</div>
			<div class="clearboth"></div>
		</div>
	</div>
</div>