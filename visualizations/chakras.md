<div style="padding-bottom: 20px;">The chakra visualizer is a way to show relative health in different energetic regions.  This can be used as a feedback tool for things like chakras, or on a map to give a more dynamic feedback.</div>
<div id="chakras" style="height:80vh;position: relative;"></div>
<div>
	<pre class="code">
		window.chakrabody=new chakras($('#chakras'),{
            chakras:{
                crown:{
                    intensity:7
                },
                thirdeye:{
                    intensity:6
                },
                throat:{
                    intensity:5
                },
                heart:{
                    intensity:4
                },
                solarplexus:{
                    intensity:3
                },
                sacral:{
                    intensity:2
                },
                root:{
                    intensity:1
                }
            }
        });
		chakrabody.start();
	</pre>
</div>
<div>
    To use or modify this code, view our <span class="task bold" data-task="link" data-intent="https://repo.nectar.earth/ekko/chakras">open source repo</span>.
</div>
<div class="bottombuttons">
    <div class="task button pinkbtn button_left" data-task="gotohash" data-intent="#/visualizations/flowers">Back: Flowers</div>
<!--     <div class="task button bluebtn button_right" data-task="gotohash" data-intent="#/visualizations/chakras">Next: Chakras</div>
 -->    <div class="clearboth"></div>
</div>
<script>
	phi.call('require',{
		js:['https://s3.amazonaws.com/wearenectar/static/chakras.js']
	},function(){
		window.chakrabody=new chakras($('#chakras'),{
            chakras:{
                crown:{
                    intensity:7
                },
                thirdeye:{
                    intensity:6
                },
                throat:{
                    intensity:5
                },
                heart:{
                    intensity:4
                },
                solarplexus:{
                    intensity:3
                },
                sacral:{
                    intensity:2
                },
                root:{
                    intensity:1
                }
            }
        });
		chakrabody.start();
	})
</script>