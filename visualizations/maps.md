<div>
	<p>Here is a map that shows the global heatmap of Nectar Users based on the home city they share.</p>
</div>
<div style="height:70vh;position: relative;">
	<iframe src="https://maps.nectar.earth/nectar-users" style="width:100%;height:100%;border:0;background: #eee"></iframe>
</div>
<div class="landscapediv fitimg" style="margin-top:50px;background-image:url(https://wearenectar.s3.dualstack.us-east-1.amazonaws.com/img/ab6708ef5e7dca/full.png">
</div>
<div>
	<p>We use this map in a <a href="https://nectar.earth/callouts" target="_blank">hybrid view</a> showing the users in the area when when you pan around the map.</p>
</div>
<div class="bottombuttons">
	<div class="task button pinkbtn button_left" data-task="gotohash" data-intent="#/visualizations">Back: Visualizations</div>
	<div class="task button bluebtn button_right" data-task="gotohash" data-intent="#/visualizations/wordcloud">Next: Word Clouds</div>
	<div class="clearboth"></div>
</div>