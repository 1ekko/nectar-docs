<div style="padding-bottom:50px;">
	Heat maps are a cool way to get an overview of the geographical distribution of things. 
</div>
<div class="landscapediv fitimg" style="background-image:url(https://wearenectar.s3.dualstack.us-east-1.amazonaws.com/img/f15c9b5fa7364f/full.png">
</div>
<div style="padding-bottom:50px;padding-top:5px;"><b>Heatmap of Intentional Communities around the world.</b> <a href="https://maps.nectar.earth/communities" target="_blank">View Map</a><br/>Sources: <a href="https://ic.org" target="_blank">FIC</a>, <a href="https://ecovillage.org/" target="_blank">GEN</a>, and <a href="https://numundo.org" target="_blank">Numundo</a></div>
<div class="landscapediv fitimg" style="background-image:url(https://wearenectar.s3.dualstack.us-east-1.amazonaws.com/img/573ac6bc862b7d/full.png">
</div>
<div style="padding-bottom:50px;padding-top:5px;"><b>Heatmap of Yoga Studios around the world.</b> <a href="https://maps.nectar.earth/yoga" target="_blank">View Map</a><br/>Sources: <a href="https://www.yogaalliance.org/Directory" target="_blank">Yoga Alliance</a></div>
<div class="landscapediv fitimg" style="background-image:url(https://wearenectar.s3.dualstack.us-east-1.amazonaws.com/img/9181bec4ead27e/full.png">
</div>
<div style="padding-bottom:50px;padding-top:5px;"><b>Heatmap of retreats in the US.</b> <a href="https://maps.nectar.earth/retreats" target="_blank">View Map</a><br/>Sources: <a href="https://www.yogaalliance.org/Directory" target="_blank">Yoga Alliance</a></div>
<div class="landscapediv fitimg" style="background-image:url(https://wearenectar.s3.dualstack.us-east-1.amazonaws.com/img/91605e2fb451b5/full.png">
</div>
<div style="padding-bottom:50px;padding-top:5px;"><b>Heatmap of spiritual centers in the western US.</b> <a href="https://maps.nectar.earth/spiritualcenters" target="_blank">View Map</a><br/>Sources: <a href="https://ourcsl.org/" target="_blank">Centers for Spiritual Living</a></div>
<div class="bottombuttons">
	<div class="task button pinkbtn button_left" data-task="gotohash" data-intent="#/visualizations/maps">Back: Maps</div>
	<div class="task button bluebtn button_right" data-task="gotohash" data-intent="#/visualizations/wordcloud">Next: Word Cloud</div>
	<div class="clearboth"></div>
</div>