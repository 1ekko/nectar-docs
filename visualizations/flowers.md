<div style="padding-bottom:100px;">
	<p>A flower is a simple way to see size relationships.  This is a flower that represents the files and lines of code of each file of the Nectar codebase.</p>
	<p>You can view a current code flower <a href="https://flower.nectar.earth" target="_blank">here</a>.</p>
</div>
<div class="landscapediv fitimg" style="background-image:url(https://wearenectar.s3.dualstack.us-east-1.amazonaws.com/img/f68423372a1adb/full.png)">
</div>
<div class="bottombuttons">
	<div class="task button pinkbtn button_left" data-task="gotohash" data-intent="#/visualizations/wordcloud">Back: Word Cloud</div>
	<div class="task button bluebtn button_right" data-task="gotohash" data-intent="#/visualizations/chakras">Next: Chakras</div>
	<div class="clearboth"></div>
</div>