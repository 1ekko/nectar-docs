# Nectar Documentation

Nectar documentation is meant to provide a in depth overview of all of Nectar's Systems.  This includes code as well as philosophy and things like governance. 

You can view a live version of the docs [here](https://docs.nectar.earth)

# Contributing to Documentation

## Clone this repo

	git clone https://repo.nectar.earth/ekko/nectar-docs.git

## Create a branch to make changes to

	cd nectar-docs
	git checkout -b [branch_name]

## Install Docsify CLI :: requires [Node.js](https://nodejs.org/en/download/package-manager/)

	npm install -g docsify-cli

## Start the Docsify Server (from within nectar-docs)

	docsify serve

## View the docs at <a href="http://localhost:3000">http://localhost:3000</a>

Any changes you make to documents will automatically sync to your browser.

## Please commit changes and make a pull request to merge changes

	git add .
	git commit -am '[enter a description of what changes you made]'
	git push origin [branch_name]


Questions, comments, or issues?  Contact [Tom](mailto:tom@nectar.earth)

With Love,

❤️