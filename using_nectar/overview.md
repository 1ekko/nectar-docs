<div style="padding-bottom:20px;">
	<div class="landscapediv coverimg" style="background-image:url(https://s3.amazonaws.com/wearenectar/static/iss.jpg)">
	</div>
</div>
<div class="strong">A High Level Overview</div>
<p>
	Nectar has many tools designed and crafted with the intention of better connecting you to the resources and information that will best serve you (and us!) in our ability to meaningfully relate to the world.
</p>
<p>
	Our intention is to simplify and bring together the tools we need to effectively communicate, coordinate, and co-create.
</p>
<p>
	Below is a chart that outlines the different functionality we are looking to bring together as well as the current status of these tools.
</p>
<p>
	We currently offer a <a href="https://nectar.earth/download" target="_blank">mobile app</a> as well as a web app.
</p>
<div>
	<table style="width:100%;color:#555">
		<tr style="background: #ddd;font-weight: bold">
			<td style="width:70px;text-align: center;vertical-align: top">
				Status
			</td>
			<td style="width:120px;text-align: left;vertical-align: top;">
				Tool
			</td>
			<td>
				Function
			</td>
		</tr>
		<tr>
			<td style="width:70px;text-align: center;vertical-align: top">
				<span style="color:green;font-weight: bold">Active</span>
			</td>
			<td style="width:120px;text-align: left;vertical-align: top;">
				<a href="#/using_nectar/chat">Chat</a>
			</td>
			<td style="vertical-align: top">
				Chat is core, basic service.  It gives people a way to communicate in various ways.
			</td>
		</tr>
		<tr>
			<td style="width:70px;text-align: center;vertical-align: top">
				<span style="color:green;font-weight: bold">Active</span>
			</td>
			<td style="width:120px;text-align: left;vertical-align: top;">
				<a href="#/using_nectar/profiles">Profile</a>
			</td>
			<td style="vertical-align: top">
				Profiles give the ability to share about yourself.  Its an online reflection of yourself, how you may digitally meet someone.  This can include photos, posts, videos, ways you like to connect, etc.
			</td>
		</tr>
		<tr>
			<td style="width:70px;text-align: center;vertical-align: top">
				<span style="color:green;font-weight: bold">Active</span>
			</td>
			<td style="width:120px;text-align: left;vertical-align: top;">
				<a href="#/using_nectar/connect">Connect</a>
			</td>
			<td style="vertical-align: top">
				Connect is a tool to better support you finding friends to connect with.  It is oriented around purpose.  What do you want to do more of?  By putting that out and intelligent tagging, it enables you to do more of what you love and connect with more people.
			</td>
		</tr>
		<tr>
			<td style="width:70px;text-align: center;vertical-align: top">
				<span style="color:green;font-weight: bold">Active</span>
			</td>
			<td style="width:120px;text-align: left;vertical-align: top;">
				<a href="#/using_nectar/pages">Pages</a>
			</td>
			<td style="vertical-align: top">
				Pages are about community.  They can be groups, services, products, etc.  Anything you may for a group or community around.
			</td>
		</tr>
		<tr>
			<td style="width:70px;text-align: center;vertical-align: top">
				<span style="color:green;font-weight: bold">Active</span>
			</td>
			<td style="width:120px;text-align: left;vertical-align: top;">
				<a href="#/using_nectar/events">Events</a>
			</td>
			<td style="vertical-align: top">
				Events allow you to organize for a specific event.
			</td>
		</tr>
		<tr>
			<td style="width:70px;text-align: center;vertical-align: top">
				<span style="color:green;font-weight: bold">Active</span>
			</td>
			<td style="width:120px;text-align: left;vertical-align: top;">
				<a href="#/using_nectar/bookmarks">Bookmarks</a>
			</td>
			<td style="vertical-align: top">
				Bookmarking across nectar gives the ability to save content for yourself as well as being able to share it with friends.
			</td>
		</tr>
		<tr>
			<td style="width:70px;text-align: center;vertical-align: top">
				<span style="color:green;font-weight: bold">Active</span>
			</td>
			<td style="width:120px;text-align: left;vertical-align: top;">
				<a href="#/using_nectar/bookmarks">Directory</a>
			</td>
			<td style="vertical-align: top">
				The Directory gives the ability to browse and filter all the pages.
			</td>
		</tr>
		<tr>
			<td style="width:70px;text-align: center;vertical-align: top">
				<span style="color:green;font-weight: bold">Active</span>
			</td>
			<td style="width:120px;text-align: left;vertical-align: top;">
				<a href="#/using_nectar/ticketing">Ticketing</a>
			</td>
			<td style="vertical-align: top">
				If you are hosting an event and would like to offer tickets, we have a ticket checkout system for you.  Simple and elegant.
			</td>
		</tr>
		<tr>
			<td style="width:70px;text-align: center;vertical-align: top">
				<span style="color:green;font-weight: bold">Active</span>
			</td>
			<td style="width:120px;text-align: left;vertical-align: top;">
				<a href="#/using_nectar/fundraising">Fund Raising</a>
			</td>
			<td style="vertical-align: top">
				Events can be fund raisers too!
			</td>
		</tr>
		<tr>
			<td style="width:70px;text-align: center;vertical-align: top">
				<span style="color:green;font-weight: bold">Active</span>
			</td>
			<td style="width:120px;text-align: left;vertical-align: top;">
				<a href="#/using_nectar/reflections">Reflections</a>
			</td>
			<td style="vertical-align: top">
				See your friend do something awesome that would generally go unnoticed or unsaid? Or just deeply appreciate them for all the ways that help you?  Leave a reflection!
			</td>
		</tr>
		<tr>
			<td style="width:70px;text-align: center;vertical-align: top">
				<span style="color:green;font-weight: bold">Active</span>
			</td>
			<td style="width:120px;text-align: left;vertical-align: top;">
				<a href="#/using_nectar/mapping">Mapping</a>
			</td>
			<td style="vertical-align: top">
				An advanced mapping system
			</td>
		</tr>
		<tr>
			<td style="width:70px;text-align: center;vertical-align: top">
				<span style="color:green;font-weight: bold">Active</span>
			</td>
			<td style="width:120px;text-align: left;vertical-align: top;">
				<a href="#/using_nectar/wallet">Wallet</a>
			</td>
			<td style="vertical-align: top">
				Wallet
			</td>
		</tr>
		<tr>
			<td style="width:70px;text-align: center;vertical-align: top">
				<span style="color:orange;font-weight: bold">In Development</span>
			</td>
			<td style="width:120px;text-align: left;vertical-align: top;">
				<a href="#/using_nectar/wallet">Marketplace</a>
			</td>
			<td style="vertical-align: top">
				Marketplace
			</td>
		</tr>
		<tr>
			<td style="width:70px;text-align: center;vertical-align: top">
				<span style="color:orange;font-weight: bold">In Development</span>
			</td>
			<td style="width:120px;text-align: left;vertical-align: top;">
				<a href="#/using_nectar/right_relation">Right Relation Tool</a>
			</td>
			<td style="vertical-align: top">
				Right Relation Tool
			</td>
		</tr>
	</table>
</div>
<div class="bottombuttons">
	<div class="task button bluebtn button_right" data-task="gotohash" data-intent="#/visualizations">Next: Visualizations</div>
	<div class="clearboth"></div>
</div>