<div>

# Philosophy

The ancient question of why run deep in our society.  Humans have a natural tendency to want to learn or explore the why.  Why do we exist? Why is the sky blue?  Why are there stars in the sky?  Why points to the understanding of the fundamental nature of knowledge, reality, and existence.

Well, why are we making Nectar?  Great question!  Let me share with you some philosophy that guides our why.

# Evolution

It is not the strongest species that survive, nor the most intelligent, but the ones most responsive to change.  

The greatest example of this is bacteria.  Considered to be the first type of life-form on this planet.  Why have they survived the entire time on this planet?  Because they are amazing at adapting to change!  Bacteria do something called horizontal gene transfer.  Horizontal gene transfer means that when one of the bacteria figures out how to adapt to the change, it freely shares it "solution", its DNA with the other bacteria so they can adapt too.

So, lets learn from the bacteria.  Lets put adaptation as a priority.  Lets put the free sharing of information as a priority.  Lets put transparency as a priority.  Let promote better "horizontal gene transfer" in our culture, lets learn from each other!

Lets learn from the patterns of our evolution so we may more consciously evolve going forward.  

"Those who fail to learn from history are doomed to repeat it."


# Evolutionary Imperative

The evolutionary imperative is the drive of the universe to go to higher orders of synergy and connection.  We can see this on our journey from bacteria, to cells, to multi-cellular beings.  Each underlaying level necessary for the next, going to greater levels of diversity, complexity, and synergy in its experience.  The 30 Trillion cells in our bodies have very complicated (yet beautifully elegant) systems to ensure the 30 trillion cells in each body are all working in concert, ensuring <b>all</b> of the cells are taken care to the best of the wholes' ability.  The stomach digests food so the whole body can reap the benefits of the nutrients.  Our stomachs are also a size relative to the size of the rest of the body to ensure it is not wasting energy.  And if you have a group of cells in a body that are going for their own self interest without regard to the whole it is a part of, you would call it a cancer.

We have created systems on our planet that indicate that we are at the crux of an evolutionary jump.  We see the recent formation of two systems known to be integral for higher level organizing principles.  A global resource distribution system (circulatory system) and a global communication system (central nervous system).  In fact, these systems have been the primary formative forces of our society in the past hundred or so years.  Think how families like the Rockefellers tapped into enormous wealth by tapping into the oil supply of the planet. And how we can now know within 10 seconds if an earth quake happens on the other side of the planet.  We have tapped into great powers, and if they are not used for the benefit of the whole, the benefit to the few becomes great, and unbalanced. 

# Endosymbiotic Evolution

We are at an evolutionary jumping point, so maybe learning from another evolutionary jumping point would be helpful. Endo-(within)-Symbiotic(union for life of two different organisms based on mutually benefit) Evolution.  This was the time in our evolutionary past where prokaryotes started to be integrated into eukaryotes.  Prokaryotes being very simple and doing basic tasks (think viruses or bacteria).  Many organelles within cells we know now (eg mitochondria and chlorophyll) started off as bacteria.  Heres a [great video](https://www.youtube.com/watch?v=9i7kAt97XYU) covering the theory of Endosymbiotic Evolution.

<div class="contentbox">
<div>
<b>Prokaryotes</b> are a kingdom that consists of mainly single-celled organisms with no distinct nucleus or organelles contained by a membrane. They can be broken into two domains: bacteria and archaea. It is believed that prokaryotes were the only organisms on Earth for millions of years.
</div>
<div>
<b>Eukaryotes</b> are organisms consisting of one or more cells that contain a nucleus and organelles contained by a membrane. They can be organized into complex structures such as humans. All living things except for prokaryotes (bacteria and archaea) are eukaryotes.
</div>
</div>

Essentially endosymbiotic evolution is the difference between prokaryotic and eukaryotic cells.

The World Economy is a great example of why we can learn from endosymbiotic evolution here.  There are a lot of businesses that do basic tasks.  Accounting, making food, tending to infrastructure, etc. Unfortunately most all of them have a <b>fiduciary responsibility</b> to their shareholders to put their company first, often times at the detriment to either workers or the environment.  And because they are in a Live or Die competitive, capitalistic environment, they do what it takes to survive.

Cells in your body do not (generally) need to worry about survival.  The greater whole they are a part of synergistically works together to ensure that cell is taken care of.  That is how endosymbiotic evolution works. 

So, by parts of cells working together, by cells working together, by organs working together, humans are possible.  Why does that have to be the end of the chain?  

Its not! When humans are working together, a planet is possible!

# A Planetary Birth

The greater whole has not yet been lucidly or coherently defined for us to agree on what the whole is. Its easy to say "the company I work for", or "the country I live in", and working at those levels definitely helps, but each are still fragmented.  What is the "whole" whole that everything on this planet is a part of?  The Planet!

<iframe src="https://www.youtube.com/embed/UlPnlRubTZQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="width:100%;height:50vh;"></iframe>

How do we structure our culture and society around this concept?  

Well on the scheme of things, we are probably still in utero as a planetary society.

We have things like global resource distribution and global communication infrastructures in place (circulatory and central nervous systems), but things like the gross economic inequality and global starvation in the millions per year points towards "still finding equilibrium".

So the best we can do is work towards creating agreement on what the "whole" is we are taking care of and what its (and our) basic rights are as a part of it. 

A skin cell on your hand doesnt need to worry about survival, it can specialize and be its awesome self knowing that is taken care of as part of the larger whole.  

Imagine if our world worked like our body.  People could find their uniqueness, their specialization, knowing they are taken care of.  They could focus more energy on art, self development, and leisure as opposed to survival.  

<div style="display: none">
# Quantum Consciousness

The quantum world has shown us just how much we don't know about the world and its "mechanics".  Observation effects the outcome!  How is that possible?  And what does that even mean? In a well know physics experiment called the double split experiment, they sent light through two slits and observed a ripple pattern on the other side, like ripples in a pond from a stone falling in.  However they found if they conducted the experiment while observing one of the slits, the did not see the ripple pattern.  Observing effected the outcome!  Why is this important?  
</div>
</div>
<div class="bottombuttons">
	<div class="task button pinkbtn button_left" data-task="gotohash" data-intent="#/welcome/intention">Back: Intention</div>
	<div class="task button bluebtn button_right" data-task="gotohash" data-intent="#/using_nectar">Next: Using Nectar!</div>
	<div class="clearboth"></div>
</div>