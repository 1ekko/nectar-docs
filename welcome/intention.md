<div>
	<p><span class="strong">Our intention</span> is to serve, to the best of our capacity, the evolutionary impulse in creating higher levels of synergy, order, and harmony.  To support the development, agreement, and implementation of global basic human and environmental rights.  To live, learn, and promote <a href="https://en.wikipedia.org/wiki/Sovereignty" target="_blank">Sovereignty</a> and <a href="https://en.wikipedia.org/wiki/Unity_in_diversity" target="_blank">Unity</a>.  And to actively participate in the co-creation a planet that works for all life.</p><!-- And to facilitate social cohesion, play, and safety. -->
	<div>
	<p>
		<span class="strong">We believe</span> that technology, used in right relation, built with <b>positive intention</b> can aid us in the process of consciously evolving together and support more efficient and egalitarian use of resources.
	</p>
	<p>
		<span class="strong">We see</span> a world where no human goes to bed hungry at night.  We see a world where mental, emotional, physical, and spiritual wellbeing are all acknowledged and supported.  We see a world coming together and celebrating the unique Art that each of us individually (and collectively) create.  We see a world where birth, life, death, and the circle of life are honored not just in humans, but in all the things we create.
	</p>
	<p>
		<span class="strong">We stand</span> strong in our commitment to the realization of a world that works for all life.
	</p>
	<p>
		<span class="strong">We hope</span> you share these intentions and visions of a world that can work for everyone. 
	</p>
	</div>
</div>
<div class="bottombuttons">
	<div class="task button pinkbtn button_left" data-task="gotohash" data-intent="#/welcome/intro">Back: Intro</div>
	<div class="task button bluebtn button_right" data-task="gotohash" data-intent="#/welcome/philosophy">Next: Philosophy</div>
	<div class="clearboth"></div>
</div>