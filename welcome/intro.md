<div>
	<div>
		<p class="strong"><b>Your Nectar is beautiful!</b></p>
		<p>
			Everyone has a uniqueness about them, their Nectar, their essence, their beauty.  We also share the story of what happened and what will happen on Earth.  This fierce contrast of uniqueness and commonality projects the beauty of our experience.  What happens when we find the balance between these?  What happens when everyone can be celebrated and supported for exactly who they are?  What happens when we can come together as a planet and agree on basic human, animal, and environmental rights?  
		</p>
		<p>
			This the heart of Nectar, to support and co-evolve with the planetary civilization that is emerging.  To celebrate our uniqueness and to focus the power of our Unity.  Only together, with our unique offerings, can we make the world our hearts know is possible...possible.
		</p>
		<p style="display: none;">So who are we?</p>
	</div>
	<div class="s-corner-all greybg contentbox">
		We are here.<br/>
		We are waking up now, out of the past, to dream a bigger dream.<br/>
		We are friends and equals, we are diverse and unique, and we're united for something bigger than our differences.<br/>
		We believe in freedom and cooperation, abundance and harmony.<br/>
		We are a culture emerging, a renaissance of the essence of humanity.<br/>
		We find our own guidance, and we discern our own truth.<br/>
		We go in many directions, and yet we refuse to disperse.<br/>
		We have many names, we speak many languages.<br/>
		We are local, we are global.<br/>
		We are in all regions of the world, we're everywhere in the air.<br/>
		We are universe being aware of itself, we are the wave of evolution.<br/>
		We are in every child's eyes, we face the unknown with wonder and excitement.<br/>
		We are messengers from the future, living in the present.<br/>
		We come from silence, and we speak our truth.<br/>
		We cannot be quieted, because our voice is within everyone.<br/>
		We have no enemies, no boundaries can hold us.<br/>
		We respect the cycles and expressions of nature, because we are nature.<br/>
		We don't play to win, we play to live and learn.<br/>
		We act out of inspiration, love and integrity.<br/>
		We explore, we discover, we feel, and we laugh.<br/>
		We are building a world that works for everyone.<br/>
		We endeavor to live our lives to their fullest potential.<br/>
		We are independent, self-sufficient and responsible.<br/>
		We relate to each other in peace, with compassion and respect, we unite in community.<br/>
		We celebrate the wholeness within and around us all.<br/>
		We dance to the rhythm of creation.<br/>
		We weave the threads of the new times.<br/>
		We are the new civilization.<br/><br/>
		<div style="text-align: left;padding-left:10px;">
		- <a href="http://www.worldtrans.org/" target="_blank">Flemming Funch</a>
		</div>
	</div>
	<div class="bottombuttons">
		<!-- <div class="task button pinkbtn button_left" data-task="gotohash" data-intent="#/welcome/vision">Back: Our Vision</div> -->
		<div class="task button bluebtn button_right" data-task="gotohash" data-intent="#/welcome/intention">Next: Intention</div>
		<div class="clearboth"></div>
	</div>
</div>