# Why Nectar?

The hallmark of all evolving life is that there is some sort of feedback and co-evolution. The Internet will be absolutely integral for this which is why it is absolutely imperative that we shift our resources to the development of technologies that are in service to the "whole".

We believe Nectar is necessary as it offers a container for the evolution of dialogue and technologies to support a planetary society.  Without agreement of the greater whole we are working on, we will at some level always be competing against each other. 

# Problem Overview

"There are a thousand hacking at the branches of evil to one chopping at the root" - Henry David Thorough.

There are a lot of issues in the world right now.  Take your pick, environmental issues, economic issues, educational issues, government/organizational issues, racial issues...there are a lot right now.  What is the common root to all of these?  Simply put, a lack of connection.  
Lack of connection to self, to the food we eat, to the elderly, to the young, to people of different backgrounds or sexual orientations or ethnicities...  This is the fundamental problem and the problem that Nectar is dedicated to addressing.


# Systemic Solutions